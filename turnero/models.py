# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class BocasAtencion(models.Model):
    id = models.BigIntegerField(primary_key=True)
    descripcion = models.CharField(max_length=200)
    activo = models.BooleanField()
    servicio = models.ForeignKey('Servicios', models.DO_NOTHING, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'bocas_atencion'


class Clientes(models.Model):
    id = models.BigIntegerField(primary_key=True)
    ultima_modificacion = models.DateTimeField()
    fecha_creacion = models.DateTimeField()
    persona = models.ForeignKey('Personas', models.DO_NOTHING, on_delete=models.CASCADE)
    prioridad = models.ForeignKey('Prioridades', models.DO_NOTHING, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'clientes'


class Personas(models.Model):
    id = models.BigAutoField(primary_key=True)
    numero_documento = models.CharField(max_length=20)
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    telefonos = models.CharField(max_length=100, blank=True, null=True)
    direccion = models.CharField(max_length=-1, blank=True, null=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    ultima_modificacion = models.DateTimeField()
    fecha_creacion = models.DateTimeField()

    class Meta:
        managed = True
        db_table = 'personas'


class Prioridades(models.Model):
    id = models.BigAutoField(primary_key=True)
    condicion = models.CharField(max_length=200)
    prioridad = models.IntegerField()
    ultima_modificacion = models.DateTimeField()
    fecha_creacion = models.DateTimeField()

    class Meta:
        managed = True
        db_table = 'prioridades'


class Servicios(models.Model):
    id = models.BigAutoField(primary_key=True)
    descripcion = models.CharField(max_length=200)

    class Meta:
        managed = True
        db_table = 'servicios'


class Turnos(models.Model):
    id = models.BigIntegerField(primary_key=True)
    usuario_asesor = models.ForeignKey('Usuarios', models.DO_NOTHING, on_delete=models.CASCADE)
    fecha_hora_llegada = models.DateTimeField()
    fecha_hora_llamada = models.DateTimeField(blank=True, null=True)
    fecha_hora_finalizacion = models.DateTimeField(blank=True, null=True)
    estado = models.CharField(max_length=-1)
    prioridad = models.ForeignKey(Prioridades, models.DO_NOTHING, on_delete=models.CASCADE)
    cliente = models.ForeignKey(Clientes, models.DO_NOTHING, on_delete=models.CASCADE)
    usuario_atencion = models.ForeignKey('Usuarios', models.DO_NOTHING, on_delete=models.CASCADE)
    boca_atencion = models.ForeignKey(BocasAtencion, models.DO_NOTHING, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'turnos'


class Usuarios(models.Model):
    id = models.BigIntegerField(primary_key=True)
    rol = models.CharField(max_length=20)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=100)
    ultima_modificacion = models.DateTimeField()
    fecha_creacion = models.DateTimeField()
    activo = models.BooleanField()
    persona = models.ForeignKey(Personas, models.DO_NOTHING, on_delete=models.CASCADE)
    boca_atencion = models.ForeignKey(BocasAtencion, models.DO_NOTHING, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'usuarios'
